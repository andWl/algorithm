from sys import stdin
input = stdin.readline
from collections import deque

n= int(input())
max = 1000000
q=deque()
a=[]

for i in range(1, n+5):
        q.append((i+1, i*(i+1)))
        a.append(i*(i+1))

while q:
    _a=q.popleft()
    index, ca =_a[0], _a[1]

    if index<n:
        a.append(ca*(index+1))
        q.append((index+1, ca*(index+1)))

a=list(set(a))

b=sorted(a)
# print(b)
print(b[n-1])

