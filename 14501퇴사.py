## 백준 14501 퇴사
from sys import stdin
input = stdin.readline
from collections import deque

n=int(input())
a=[list(map(int, input().split())) for _ in range(n)]

q=deque()


def main():
    ans=0


    for k in range(n):
        if k + a[k][0] <= n:
            q.append((k+a[k][0], a[k][1]))
        while (q):
            _q=q.popleft()
            time, sum_p= _q[0],_q[1]
            change = False
            for i in range(n):
                if i < time:
                    continue

                if i+a[i][0] <= n:
                    change =True
                    q.append((i+a[i][0], sum_p+a[i][1]))

            if not change:
                ans=max(ans,sum_p)

    print(ans)

main()
