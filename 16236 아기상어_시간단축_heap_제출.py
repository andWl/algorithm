## 백준 16236 아기 상어 시간초과
from sys import stdin
input = stdin.readline
from collections import deque
from heapq import heappop
from heapq import heappush
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]
dx,dy=(-1,0,1,0),(0,1,0,-1)
check = [[False] * n for _ in range(n)]
q,size,eat,ans,first_x,first_y=[],2,0,0,0,0
timer=0
for i in range(n):
    for j in range(n):
        if a[i][j] == 9:
            heappush(q,(0,i,j))
            first_x, first_y = i, j
# q=sorted(q)
# print(q)
# move_q=deque()
def _print():
    for i in range(n):
        for j in range(n):
            print(a[i][j], end=' ')
        print()

def bite(_timer,_x,_y):
    global x,y,eat,size,ans,first_x,first_y
    eat+=1
    a[first_x][first_y],x,y=0,_x,_y
    if eat == size:
        size+=1
        eat=0
    a[x][y],ans,first_x,first_y = 9,timer+_timer,x,y
while q:
    d,x,y= heappop(q)

    if 0<a[x][y]<size:
        bite(d,x,y)
        q.clear()
        check = [[False] * n for _ in range(n)]
        _print()

    for i in range(4):
        # _x,_y는 이동하고자 하는 위치
        _x, _y = x + dx[i], y + dy[i]
        if _x >= 0 and _x < n and _y >= 0 and _y < n:
            # 같거나, 0(빈공간), 먹을수 있는 생선이거나
            # check 하면 안된다.
            if a[_x][_y] <= size and not check[_x][_y]:
                # 이동을 한번 한거다 먹든 말든
                heappush(q,(d + 1,_x, _y))
                check[_x][_y] = True


    # print("ans", ans)
print(ans)