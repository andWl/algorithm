# 6
# 1 2 3 4 5 6
# 2 1 1 1

#
# 6
# 1 2 3 4 5 6
# 0 0 4 1

from sys import stdin
import math
from itertools import permutations
input=stdin.readline
cc=0
n=int(input())
num=list(map(int,input().split()))
cal=list(map(int,input().split()))

# 여기서 -1e9 , 1e9 하니깐 맞네; 그전에는 틀림
# 아.... 최대값이 음수일수도 있는데 am_ans 를 0으로 잡아놔서 음수인경우도 0을출력.....
ma_ans,mi_ans,l_cal = -1e9,1e9,[]

for i in range(len(cal)):
    for _ in range(cal[i]):
        l_cal.append(i)
t_cal=list(permutations(l_cal,len(l_cal)))
# 여기서 리스트로 안만들면 {} 요걸로 감싸짐
set_t_cal=list(set(t_cal))

def calc(_l_cal):
    global ma_ans, mi_ans
    s=num[0]

    for i in range(len(_l_cal)):
        if _l_cal[i]==0:
           s += num[i + 1]
        elif _l_cal[i]==1:
            s -= num[i + 1]
        elif _l_cal[i] == 2:
            s *= num[i + 1]
        else:
            if s<0:
                s=math.ceil(s/num[i+1])
                # s*=-1
                # s=math.floor(s/num[i+1])
                # s*=-1
            else:
                s=math.floor(s/num[i+1])
        # print(s)
    # print(s)
    ma_ans=max(ma_ans,s)
    mi_ans=min(mi_ans,s)

def main():
    for i in range(len(set_t_cal)):
        # print(num)
        # print(set_t_cal[i])
        calc(set_t_cal[i])


main()
print(ma_ans)
print(mi_ans)