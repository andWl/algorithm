from sys import stdin
input = stdin.readline
from itertools import combinations

n= int(input())
a= [list(map(int, input().split())) for _ in range(n)]
mem,t_b,ans=[],[],101
# 여기서 또 실수함. 0 으로 해버림


for i in range(n):
    mem.append(i)

t=list(combinations(mem,int(n/2)))

def calc(_ta,_tb):
    global ans
    s_a, s_b=0,0

    for i in range(int(n/2)):
        for j in range(int(n/2)):
            # print(i,j)
            s_a+=a[_ta[i]][_ta[j]]
            s_b+=a[_tb[i]][_tb[j]]
    ans=min(ans,abs(s_a-s_b))

def main():
    for i in range(len(t)):
        t_b=[]
        for j in range(n):
            for k in range(int(n/2)):
                if mem[j]==t[i][k]:
                    break
            if mem[j] not in t[i]:
                t_b.append(mem[j])
        calc(t[i],t_b)

main()
print(ans)
