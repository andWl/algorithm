from sys import stdin
from itertools import combinations
input = stdin.readline

n, m = map(int, input().split())
a = [list(map(int, input().split())) for _ in range(n)]

home, chi , dis,dis_home, ans= [],[],0,[],1e9

for i in range(n):
    for j in range(n):
        t = a[i][j]
        if t==1:
            home.append((i,j))
        elif t==2:
            chi.append((i,j))

# print(chi)
# com_chi=list(combinations(chi,1))
# print(com_chi)
#
# com_chi=list(combinations(chi,2))
# print(com_chi)

# 하나의 치킨 집 세트가 온다
def calc(_chi):
    # print(_chi)
    global dis,dis_home
    # for 문에 index, 리스트 값 둘다 표현 가능한지
    for i in home:
        min_dis = 1e9
        # 각각의 집에서 치킨집 거리 최소값 찾는다
        for j in _chi:
            # print("cxccc", j)
            min_dis =min(min_dis, abs(i[0] - j[0]) + abs(i[1] - j[1]))
        dis+=min_dis





def dis_sum():
    global dis
    sum=0
    for i in range(len(dis)):
        sum+=dis[i]
    return sum

def main():
    global ans, dis
    # 치킨집 수를 정함
    for k in range(1, m+1):
        # 치킨집 1,2,3 .. 일때 combination
        for i in list(combinations(chi,k)):
            #1,2,3 각각 일때 거리를 구하는거
            # print("i   ", i)
            calc(i)
            #
            # print("dis:", dis)
            ans=min(ans,dis)
            dis=0


main()
print(ans)