# global 선언의 중요성

from sys import stdin
input = stdin.readline
from copy import deepcopy
from collections import deque
from time import sleep
n, m = map(int, input().split())
a = [list(map(int, input().split())) for _ in range(n)]
ans = 0
dx, dy = (-1,0,1,0), (0,1,0,-1)
q=deque()
# 0위치, virus 위치
# temp 도 이렇게 2차원으로 만들어줘야함
_a = list([0]*m for _ in range(n))


z, v =[],[]
for i in range(n):
    for j in range(m):
        if a[i][j]==0:
            z.append((i,j))
        if a[i][j]==2:
            v.append(((i,j)))

def case(_x,_y):
    global _a
    if _x>=0 and _x<n and _y>=0 and _y<m:
        if _a[_x][_y] == 0 :

            _a[_x][_y] = 2
            q.append((_x, _y))


def move(_v):

    q.append(_v)
    while q:
        _vt=q.popleft()
        x, y = _vt[0],_vt[1]

        _x, _y = x + dx[0], y + dy[0]
        case(_x,_y)

        _x, _y = x + dx[1], y + dy[1]
        case(_x, _y)

        _x, _y = x + dx[2], y + dy[2]
        case(_x, _y)

        _x, _y = x + dx[3], y + dy[3]
        case(_x, _y)

def virus():
    for i in range(len(v)):

        move((v[i][0], v[i][1]))

def count():
    ct=0
    global ans
    for i in range(n):
        for j in range(m):
            if _a[i][j] == 0:
                ct+=1
    ans=max(ans, ct)

def _print():
    for i in range(n):
        for j in range(m):
            print(_a[i][j], end='')
        print()
    sleep(2)
    print("***********")


def main():
    global _a
    for i in range(len(z)):
        for j in range(i+1,len(z)):
            for k in range(j+1,len(z)):
                # print((i,j,k))
                _a=deepcopy(a)
                _a[z[i][0]][z[i][1]],_a[z[j][0]][z[j][1]],_a[z[k][0]][z[k][1]]=1,1,1

                virus()
                # _print()
                # _print()
                # _print()

                count()


main()
print(ans)
