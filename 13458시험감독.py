from sys import stdin
import math
input=stdin.readline

n, sum =int(input()),0
A=list(map(int, input().split()))
b,c=map(int, input().split())

for i in range(n):
	# A[i]-b 이런 계산은 반드시 이렇게 조건을 넣어줘야함
	if A[i] >=b:
		sum+=math.ceil((A[i]-b)/c)

print(sum+n)

