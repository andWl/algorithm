# 5 5
# # #####
# # #..B#
# # #.#.#
# # #RO.#
# # #####
#
# #
# # 7 7
# # #######
# # #...RB#
# # #.#####
# # #.....#
# # #####.#
# # #O....#
# # #######
from collections import deque
from sys import stdin
input=stdin.readline

n, m = map(int, input().split())
metrix=[list(input().strip()) for _ in range(n)]
q = deque()

check=[[[[False]*m for _ in range(n)] for _ in range(m)] for _ in range(n)]

dx, dy=(-1,0,1,0),(0,1,0,-1)

for i in range(n):
    for j in range(m):
        if metrix[i][j] == "R":
            _rdx, _rdy = i, j
        if metrix[i][j] == "B":
            _bdx, _bdy = i, j

q.append((_rdx,_rdy,_bdx,_bdy,0))
check[_rdx][_rdy][_bdx][_bdy]=True


def move(dx,dy,xdx,ydy):
    xc=0
    while metrix[xdx+dx][ydy+dy]!="#" and metrix[xdx][ydy]!="O":
        xdx+=dx
        ydy+=dy
        xc+=1
    return xdx, ydy, xc

# 동서남북
def bfs():
    while q:
        rdx, rdy, bdx, bdy, count=q.popleft()

        for i in range(4):

            _rdx, _rdy, rc = move(dx[i],dy[i], rdx, rdy)
            _bdx, _bdy, bc = move(dx[i],dy[i], bdx, bdy)

            if count>=10:
                print(-1)
                return

            elif metrix[_bdx][_bdy] == "O" :
                continue

            elif metrix[_rdx][_rdy] == "O":
                print(count+1)
                return
            elif _rdx == _bdx and _rdy ==_bdy:
                if rc > bc:
                    _rdx-=dx[i]
                    _rdy-=dy[i]
                else:
                    _bdx-=dx[i]
                    _bdy-=dy[i]

            if not check[_rdx][_rdy][_bdx][_bdy]:
                _count=count + 1
                check[_rdx][_rdy][_bdx][_bdy]=True
                q.append((_rdx,_rdy,_bdx,_bdy,_count))

    print(-1)
bfs()
