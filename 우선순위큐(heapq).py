from heapq import heappush, heappop

a=[]

#
# [(1, 0, 3), (1, 3, 0), (2, 3, 1), (4, 0, 0), (2, 0, 2), (3, 0, 1), (3, 3, 2), (4, 3, 3)]









heappush(a,(4, 0, 0))
heappush(a,(3, 0, 1))
heappush(a,(2, 0, 2))
heappush(a,(1, 0, 3))
heappush(a,(1, 3, 0))

heappush(a,(2, 3, 1))
heappush(a,(3, 3, 2))
heappush(a,(4, 3, 3))

print(a)
b=heappop(a)

print(b)

# 4
# 4 3 2 1
# 0 0 0 0
# 0 0 9 0
# 1 2 3 4
## 백준 16236 아기 상어 시간초과
from sys import stdin
input = stdin.readline
from heapq import heappop
from heapq import heappush
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]
q,size,eat,ans,first_x,first_y=[],2,0,0,0,0
timer=0
for i in range(n):
    for j in range(n):
        if a[i][j] not in (0,9):
            print(a[i][j],i,j)
            heappush(q, (a[i][j],i,j))
        elif a[i][j] == 9:
            first_x, first_y = i, j
print(q)
