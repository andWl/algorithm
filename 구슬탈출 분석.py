# 문제를 딱보고 bfs네? 어떤상태를 큐에 넣어야하지?]
# red , brue 공의 각각의 x,y 위치 및 움직인거리 5가지를 한 상태로 보고 큐에 넣자! 생각을 해야함
# 변경하는 놈은 리스트, 고정값은 튜플


from collections import deque



# 이건 한줄넣고 엔터 한줄넣고 엔터 말고 그냥 문제지 그대ㅗ 복사해서 넣고 싶어서 만드는거다 다른 의미가 없음
from sys import stdin

# realine() ㅇㅏ니

# 이건 문자가 안 띄어써져있는 경우!!!! readline 통해서 한줄씩 가져오는건가
input = stdin.readline



# split은 띄어 쓰기 있는거 두개 인자 동시에 받아서 각각 n,m에 넣기
# 여기서 int 를 안넣으면 1 3 을 그냥 "1 3" 으로 하나의 string으로 인식한다. int 넣어줘야함
n, m = map(int, input().split())

# 띄어쓰기 전부 공백으로 바꾸고 싶으면 ?
# % ^ % % 이런걸 %^%% 로 바꾸는건 text_Ex.replace(" ", "")

# 앞뒤 공백만, /n 제거하는건
# text_Ex.strip()

# print(n, m)

# 여려줄 입력 받아서 2차원 리스트 구현 _(언더스코어) 는 인덱스 무시하는것 (필요없을 때)
# n이 행
#strip해야 엔터(/n) 제거됨
# 밖에 [] 감싸는건 리스트안에 뭔가를 넣는다는 뜻
# 안에를 보면 먼저 리스트를 한줄 받는데 저렇게 list 안에 input이 있으면  strip으로 엔터 제거
# range(n) 행 만큼 !!! 암기!!

# 아래 테스트 해보면 그냥 list안에 input을 받아버리면 숫자던, 문자던 하나씩 쪼개서 만들어준다
# test = list(input())
# print(test)

a = [list(input().strip()) for _ in range(n)]

# 일단 false를 다 넣는다는 뜻
# 경우의 수를 따져보면 rx, ry, bx, by가 각각 m*n 행렬 칸에 있는걸 다 곱하면 m*n^4 가 되는거다. 이 모든 경우의 수에 대해서 해당 경우일때
# 땅을 밟기때문에 true 로 만드는거다. 즉, 모든 4차 행렬에 true가 있을 필요가 없는거다. 어떤 곳은 벽이라서 못들어갈수 있기 때문.
# 즉 check는 공 두개의 위치 일 때 지난 온 길로 쓰이는거다.
# 아래를 출력해보면 m이 2 일때 [False,False]가 나온다.
# print([False]*m)
# 주의 할게 아래 순서를 보면 m,n , m,n 이다. 열행 열행
# [[False]*m for _ in range(n)] 이거를 xxx로 치환하면
# check = [["xxx" for _ in range(m)] for _ in range(n)]
# 이처럼 그냥 xxx라는 놈을 m번 리스트에 넣는거다 -> 마치 처음에 [false]를 *m 한것과 같음
# aaa=[False]*m 을 바꾸면
# aaa= [False for _ in range(3)] 이 된다


check = [[[[False]*m for _ in range(n)] for _ in range(m)] for _ in range(n)]



# 이건 반드시 기억해라, 동서남북일때 x축y축 변화가 아니다!!! 행열의 변화다  왼쪽부터 움직이는거다 그냥 기억, dx(행) 는 -1 부터 시작하고 dy(열)는 대칭
# 반드시 각각 튜플에 담는다. 변경되는게 아니기 때문에
# 위로 부터 가는거다 dx 가 -1, y가 0일 때는 위로 가는거다!! 행이 하나 줄어드는거기 때문 즉 y축 으로 올라가서 행이 증가한다고 생각하는게 아니다!
# 행열은 아래로 내려오고 옆으로 증가할수록 + 가 되는것이다. 
# 즉 위, 오른쪽, 아래, 왼쪽 순서다 

dx, dy = (-1, 0, 1, 0), (0, 1, 0, -1)

# q = deque()
#
# def init():
#     _rx, _ry, _bx, _by = [0]*4

#  /* 행열 순서로 넣는다 n, m 그리고 _rx, _ry는 (x,y) 축 을 의미하는게 아니다, 행열을 의미하는거다 */
#  /*   이렇게 순간 쓰고 말아버리는건 _bx, _by 요런 형태로 만들어주자 */
# /* 여기서 rx, ry, bx, by 위치를 찾는거다 */
#     for i in range(n):
#         for j in range(m):
#             if a[i][j] == 'R':
#                 _rx, _ry = i, j
#             elif a[i][j] == 'B':
#                 _bx, _by = i, j

#  /* queue에 넣을 때 튜플로 바로 담으면 된다. 별도로 [] 이런 공간을 만들어줄 필요가 없다. */
#     q.append((_rx, _ry, _bx, _by, 0))


# 초반에 있던 자리는 true로 변경
# rx ry bx by 의 위치는 중복되지 않는다. 정확히 한번뿐이다. 따라서 그 위치일 때
#     check[_rx][_ry][_bx][_by] = True
#
# def move(_x, _y, _dx, _dy, _c):
#     while a[_x+_dx][_y+_dy] != '#' and a[_x][_y] != 'O':
#         _x += _dx
#         _y += _dy
#         _c += 1
#     return _x, _y, _c
#
# def bfs():

# /* q  가 존재하면 계속고고 */
#     while q:


#         rx : red 의 x축 ry : red 의 y축, b는 blue , d는 현재의 공의 위치에서 움직인 거리
#         즉 큐에는 각각의 경우의 수애 대한 "현재 상태"를 가지고 있는것이다. 공의 위치 및 움직인 거리를 경우의 수마다 큐에 넣는거다.
#         주의 할 점은 행열 개념이 xy가 아니다. 그냥 무시, 행이 y이 이고 열이 x인데 그냥 x,y로 이해하는게 빠름


# deque에서 popleft는 왼쪽, pop은 오른쪽이다
# 이렇게 한방에 변수에 넣을 수 있음
# 즉, 하나의 경우의 수마다 동서남북 네방향 전부 탐색하는거다

#         rx, ry, bx, by, d = q.popleft()
#         if d >= 10:
#             break

# /* 유의 할게 이중 포문 쓰는게 아니다. dx, dy는 엄연히 짝이 4개 뿐이다. 동서남북 */
#         for i in range(4):

#             red, blue 공 위치를 각각 담는다.
#             nrx, nry, rc = move(rx, ry, dx[i], dy[i], 0)
#             nbx, nby, bc = move(bx, by, dx[i], dy[i], 0)

#/* 파란색이 먼저 들어가면 해당 for문 ㄱ*/
#             if a[nbx][nby] == 'O':
#                 continue
#             if a[nrx][nry] == 'O':
#                 print(d+1)

# /*  return 으로 그냥 종료 하는거네 왜냐하면 너비 우선 탐색이기 때문에 가장 먼저 찾은 놈을 그냥 가져가면 됨 더 이상 할 필요가 없음 */
#                 return
#             if nrx == nbx and nry == nby:
#                 if rc > bc:
#                     nrx, nry = nrx-dx[i], nry-dy[i]
#                 else:
#                     nbx, nby = nbx-dx[i], nby-dy[i]
#
#
#          해당 위치가 False가 아니면 True로 바꾸고
#             if not check[nrx][nry][nbx][nby]:
#                 check[nrx][nry][nbx][nby] = True
#                 q.append((nrx, nry, nbx, nby, d+1))
#     print(-1)
#
# init()
# bfs()




