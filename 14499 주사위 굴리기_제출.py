##백준 14499 주사위 굴리기
from sys import stdin
input=stdin.readline
from copy import deepcopy

n, m, nx, ny, cc = map(int , input().split())
met = [list(map(int, input().split())) for _ in range(n)]
r=[0]*6
com=list(map(int , input().split()))
dx, dy = (0,0,-1,1), (1,-1,0,0)

def move(d,t):
    if d==1:
        t[0], t[2], t[3], t[5] = r[3], r[0], r[5], r[2]
    elif d==2:
        t[0], t[2], t[3], t[5] = r[2], r[5], r[0], r[3]
    elif d==3:
        t[0], t[1], t[4], t[5] = r[4], r[0], r[5], r[1]
    else:
        t[0], t[1], t[4], t[5] = r[1], r[5], r[0], r[4]
    return deepcopy(t)

def change():
    if met[nx][ny]==0:
        met[nx][ny]=r[5]
    else:
        r[5]=met[nx][ny]
        met[nx][ny]=0

def main():
    global r
    global nx,ny
    for i in range(cc):
        d=com[i]
        nx += dx[d - 1]
        ny += dy[d - 1]
        if nx<0 or nx >n-1 or ny <0 or ny >m-1:
            nx -= dx[d - 1]
            ny -= dy[d - 1]
        else:
            r=move(d,deepcopy(r))
            change()
            print(r[0])

main()
