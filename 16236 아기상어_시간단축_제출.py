## 백준 16236 아기 상어 시간초과
from sys import stdin
input = stdin.readline
from collections import deque
q=deque()
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]
dx,dy,size,eat,ans,first_x,first_y=(-1,0,1,0),(0,1,0,-1),2,0,0,0,0

check = [[False]*n for _ in range(n)]
fish_eat = [[False] * n for _ in range(n)]
def init():
    global first_x, first_y
    for i in range(n):
        for j in range(n):
            if a[i][j]==9:
                first_x,first_y=i,j
                q.append((i,j,0))
                return
def bite(_x,_y,_timer):
    global x,y,eat,size,ans,first_x,first_y
    eat+=1
    a[first_x][first_y],x,y=0,_x,_y
    if eat == size:
        size+=1
        eat=0
    a[x][y],ans,first_x,first_y=9,_timer,x,y
    q.append((x,y,_timer))

def eat_cert(timer_limit):
    for i in range(n):
        for j in range(n):
            if fish_eat[i][j]:
                bite(i, j, timer_limit)
                return True
    return False

init()
while 1:
    for i in range(n):
        for j in range(n):
            check[i][j],fish_eat[i][j]=False,False
    timer_limit=1e9
    while q:
        x,y,timer=q.popleft()
        if timer == timer_limit:
            q.clear()
            break
        for i in range(4):
            _x,_y = x + dx[i], y + dy[i]
            if _x>=0 and _x<n and _y>=0 and _y<n:
                if a[_x][_y] <= size and not check[_x][_y]:
                    q.append((_x,_y,timer+1))
                    check[_x][_y]=True
                    if a[_x][_y] < size and a[_x][_y] >0:
                        timer_limit=timer+1
                        fish_eat[_x][_y]=True


    if not eat_cert(timer_limit):
        print(ans)
        break
