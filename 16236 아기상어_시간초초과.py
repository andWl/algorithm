# 6
# 1 0 0 0 0 0
# 0 0 0 0 0 5
# 9 1 0 5 6 6
# 1 0 0 3 4 5
# 3 2 0 6 5 4
# 6 6 6 6 6 6

# from sys import stdin
# input = stdin.readline
from heapq import heappush, heappop
import sys
input = sys.stdin.readline
sys.setrecursionlimit(10000)

from collections import deque

q=deque()
p=[]
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]
# print(n,a)
# 상어 위치
dx,dy,index,size,eat,ans=(-1,0,1,0),(0,1,0,-1),False,2,0,0
for i in range(n):
    for j in range(n):
        # print(i,j)
        if a[i][j]==9:
            shark_pos=(i,j)
            index=True
            break
    if index:
        break
first_x,first_y=shark_pos[0],shark_pos[1]
q.append((first_x,first_y,0))
# heappush(p,(first_x,first_y,0))


def _print():
    for i in range(n):
        for j in range(n):
            print(a[i][j], end=' ')
        print()

def bite(_x,_y,_timer):
    # 아래 생성된 글로벌 변수도 가능한가?

    global x,y,eat,size,ans,first_x,first_y
    eat+=1
    # 일단 현재 위치는 0으로 만든다
    a[first_x][first_y]=0
    # print(x,y)
    x,y=_x,_y
    if eat % size==0:
        size+=1
        eat=0
    #    먹고 나서는 9 현재 상어 위치
    a[x][y]=9
    # _print()
    ans=_timer
    # print("x,y,size,eat,_timer",x,y,size,eat,_timer)
    first_x,first_y=x,y
    q.append((x,y,_timer))
    # heappush(p,(x,y,_timer))

# def finding_fish():
while 1:
    # print("!@#!@#@#!@#!@#!@#!@#!#")
# 이거 한바퀴 돌면 한마리 먹는거다
    check = [[False]*n for _ in range(n)]
    fish,timer_limit=[],1e9
    while q:

        # 이렇게 바로 대입 가능
        x,y,timer=q.popleft()
        # x, y, timer = heappop(p)
        # print("timer, timerlimit :", timer, timer_limit)
        # timer limit 보다 많은 즉 멀리있는 놈은 굳이 찾을 필요 없음
        # 여기서 <= 로 해서 다 찾았음
        # if timer < timer_limit:
        if timer == timer_limit:
            q.clear()
            p.clear()
            break
        # print(x,y,timer)

        for i in range(4):
            # _x,_y는 이동하고자 하는 위치
            _x,_y = x + dx[i], y + dy[i]
            if _x>=0 and _x<n and _y>=0 and _y<n:
                # 같거나, 0(빈공간), 먹을수 있는 생선이거나
                # check 하면 안된다.
                if a[_x][_y] <= size and not check[_x][_y]:
                    # 이동을 한번 한거다 먹든 말든
                    q.append((_x,_y,timer+1))
                    # heappush(p,(_x,_y,timer+1))
                    check[_x][_y]=True

                    # 먹기 위한 조건
                    if a[_x][_y] < size and a[_x][_y] >0:
                        fish.append((_x,_y,timer+1))
                        timer_limit=timer+1
                        # print("timer_limit: " , timer_limit)
    # print(fish)
    min_x,min_y,up_fish,left_fish=1e9,1e9,[],[]
    # 잡을수있는 생성이 2개 이상이면
    if len(fish)>1:
        for i in fish:
            min_x=min(min_x,i[0])

        for i in fish:
            if i[0]==min_x:
                up_fish.append((i[0],i[1]))

        # 위에 있는 놈들도 2마리 이상이면
        if len(up_fish)>1:
            for i in up_fish:
                min_y = min(min_y, i[1])
            bite(min_x,min_y,fish[0][2])

        else:
            # print(up_fish)
            bite(up_fish[0][0],up_fish[0][1],fish[0][2])
    elif len(fish)==1:
        bite(fish[0][0],fish[0][1],fish[0][2])

    else:
        print(ans)
        break







    # finding_fish()