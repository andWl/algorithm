# 여기서 -1 0 1 0 , 0 1 0 -1 을 쓰는게 아니다. 이건 이동하는게 아니고 그냥 고정된 값을 가져오는거다
# 브루트 포스다

# 모든 경우의 수를 만들어서 위치 좌표를 찍으면 된다
# 0,0 기준으로 찍으면 된다.

## 백준 14500 테트로미노
from sys import stdin
imput=stdin.readline

n, m = map(int, input().split())
a=[list(map(int, input().split())) for _ in range(n)]

p=(
    ((0,1), (0,2), (0,3))
    ,((1,0), (2,0), (3,0))
    ,((0,1), (1,0), (1,1))
    , ((1, 0), (2, 0), (2, 1))
    , ((0, 1), (0, 2), (-1, 2))
    , ((0, 1), (1, 1), (2, 1))
    , ((1, 0), (0, 1), (0, 2))
    , ((1, 0), (2, 0), (2, -1))
    , ((1, 0), (1, 1), (1, 2))
    , ((0, 1), (1, 0), (2, 0))
    , ((0, 1), (0, 2), (1, 2))
    , ((1, 0), (1, 1), (2, 1))
    , ((1, 0), (1, -1), (0, 1))
    , ((0, 1), (-1, 1), (1, 0))
    , ((0, 1), (1, 1), (1, 2))
    , ((0, 1), (0, 2), (1, 1))
    , ((0, 1), (-1, 1), (0, 2))
    , ((1, 0), (2, 0), (1, -1))
    , ((1, 0), (1, 1), (2, 0))
   )
def main():
    ans = 0
    for k in range(len(p)):
        for i in range(n):
            for j in range(m):
                x = a[i][j]
                for l in range(3):
                    try:
                        x+=a[i+p[k][l][0]][j+p[k][l][1]]

                    except IndexError:
                        x=0
                        continue
                if x!=0:
                    ans=max(ans,x)
    print(ans)

main()