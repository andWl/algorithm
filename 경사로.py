# 6 2
# 3 3 2 1 1 2
# 2 3 3 3 3 3
# 2 2 2 3 2 3
# 1 1 1 2 2 2
# 1 1 1 3 3 1
# 1 1 2 3 3 2

from sys import stdin
input=stdin.readline
ans=0

n,l=map(int,input().split())
a=[list(map(int, input().split())) for _ in range(n)]
cert, l_check,same_ct,up_cnt,down_cnt,l_cnt,l_cnt_check =False, 1,1,0,0,0,False

def simul(_a,_b):
    global cert, l_check,same_ct,up_cnt,down_cnt,l_cnt,l_cnt_check

    if l_cnt_check==True:
        l_cnt+=1
    if l_cnt % l == 0:
        l_cnt_check=False

    # 다리 놓는 순간
    if l_check!=1 and _a==_b :
        if l_check == l :
            same_ct += 1
            cert, l_check,down_cnt,up_cnt,l_cnt_check = True, 1,0,0,True

    elif _a ==_b:
        cert=True
        same_ct+=1

    elif abs(_a-_b) == 1:
        if l == 1:
            cert = True
        #     올라가는데 이미 한번 올라왔으면
        elif _a < _b and up_cnt==1:
            cert=False
        #     올라가는데 지금까지 누적된 같은 높이가 l보다 크면 다리 가능
        elif _a < _b and same_ct >= l:
            cert = True
            same_ct = 1
            down_cnt, up_cnt = 0, 1
            # 다리 놓으면
            l_cnt_check=True
        # 같은게 얼마없는데 올라가는거 첨부터
        elif _a < _b and same_ct < l:
            cert=False

        #     한번 내려왔는데 또 내려가면
        elif _a > _b and down_cnt==1:
            cert=False
        #     보류
        elif _a > _b:
            l_check += 1
            down_cnt+=1
            cert=True
            same_ct=1
        else:
            print("??")
    else:
        cert=False




def main():
    global l_check, cert, same_ct,up_cnt,down_cnt
    ans=0
    for i in range(n):
        l_check, cert,same_ct,up_cnt,down_cnt, l_cnt= False, False,1,0,0,0
        for j in range(n-1):
            t_a, t_b =a[i][j], a[i][j+1]
            simul(t_a,t_b)
            if not cert:
                break
        if cert:
            ans+=1
        l_check, cert,same_ct,up_cnt,down_cnt ,l_cnt= False, False,1,0,0,0
        for k in range(n-1):
            t_a, t_b = a[k][i], a[k+1][i]
            print(t_a,t_b)
            simul(t_a,t_b)
            if not cert:
                break
        if cert:
            ans+=1
    print(ans)
main()
