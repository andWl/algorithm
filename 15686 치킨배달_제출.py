# 15686 백준 치킨배달
from sys import stdin
from itertools import combinations
input = stdin.readline
n, m = map(int, input().split())
a = [list(map(int, input().split())) for _ in range(n)]
home, chi , dis,dis_home, ans= [],[],0,[],1e9

for i in range(n):
    for j in range(n):
        t = a[i][j]
        if t==1:
            home.append((i,j))
        elif t==2:
            chi.append((i,j))

def calc(_chi):
    global dis,dis_home
    for i in home:
        min_dis = 1e9
        for j in _chi:
            min_dis =min(min_dis, abs(i[0] - j[0]) + abs(i[1] - j[1]))
        dis+=min_dis

def main():
    global ans, dis
    for k in range(1, m+1):
        for i in list(combinations(chi,k)):
            calc(i)
            ans=min(ans,dis)
            dis=0
main()
print(ans)