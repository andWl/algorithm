## 백준 16236 아기 상어 시간초과
from sys import stdin
input = stdin.readline
from heapq import heappop,heappush
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]

q =[]
first_x, first_y = 0,0
# count=0
def init():
    global first_x,first_y
    for i in range(n):
        for j in range(n):
            if a[i][j] == 9:
                heappush(q,(0,i,j))
                first_x, first_y = i, j
                return



def bfs():
    global first_x, first_y
    size, eat, ans =  2, 0, 0
    check = [[False] * n for _ in range(n)]
    while q:
        # count+=1
        d,x,y= heappop(q)

        if 0<a[x][y]<size:
            # bite(d,x,y)
            # global x, y, eat, size, ans, first_x, first_y
            eat += 1
            a[first_x][first_y]=0
            if eat == size:
                size += 1
                eat = 0
            a[x][y], ans, first_x, first_y = 9, d, x, y
            # q.clear()
            while q:
                # count += 1
                q.pop()
            check = [[False] * n for _ in range(n)]
            # _print()


        for dx, dy in (-1, 0), (0, -1), (1, 0), (0, 1):
            # count += 1
            nd, nx, ny = d+1, x+dx, y+dy
            if nx < 0 or nx >= n or ny < 0 or ny >= n:
                continue
            if 0 < a[nx][ny] > size or check[nx][ny]:
                continue
            check[nx][ny] = True
            heappush(q, (nd, nx, ny))
        #
        # for dx, dy in (-1, 0), (0, -1), (1, 0), (0, 1):
        #     # count += 1
        #     # _x,_y는 이동하고자 하는 위치
        #     _x, _y = x + dx, y + dy
        #     if _x >= 0 and _x < n and _y >= 0 and _y < n:
        #         # 같거나, 0(빈공간), 먹을수 있는 생선이거나
        #         # check 하면 안된다.
        #         if a[_x][_y] <= size and not check[_x][_y]:
        #             # 이동을 한번 한거다 먹든 말든
        #             heappush(q,(d + 1,_x, _y))
        #             check[_x][_y] = True
    print(ans)

init()
bfs()
    # print("ans", ans)
# print(count)
