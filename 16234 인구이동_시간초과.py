# 16234 인구이동 시간초과
import sys
from collections import deque
input = sys.stdin.readline
sys.setrecursionlimit(10000)
n, l, r = map(int,input().split())
a=[list(map(int,input().split())) for _ in range(n)]
check=[[9]*n for _ in range(n)]
q=deque()
dx,dy,ans,change=(-1,0,1,0),(0,1,0,-1),0,False

def _print():
    for i in range(n):
        for j in range(n):
            print(a[i][j], end=' ')
        print()

def move(check_num):
    global change
    for t in range(check_num):
        sum,cnt = 0,0
        for i in range(n):
            for j in range(n):
                if check[i][j] == t:
                    sum += a[i][j]
                    cnt+=1
        if sum>0:
            sc=sum // cnt
            for i in range(n):
                for j in range(n):
                    if check[i][j] == t:
                        a[i][j]=sc
    print("sum: ", sum)
    print("count: ", cnt)


while 1:
    check_num = 0
    for i in range(n):
        for j in range(n):
            if check[i][j]!=9:
                continue
            q.append((i,j))
            while q:
                t = q.popleft()
                for k in range(4):
                    x, y = t[0] + dx[k], t[1] + dy[k]
                    if x >= 0 and y >= 0 and x < n and y < n and check[x][y]==9:
                            temp = abs(a[t[0]][t[1]] - a[x][y])
                            if temp >= l and temp <= r:
                                check[x][y] = check_num
                                q.append((x, y))
                                change=True
            move(check_num + 1)
            check_num += 1
    check = [[9] * n for _ in range(n)]
    if change:
        _print()
        print()
        ans+=1
        change=False
    else:
        print(ans)
        break