import itertools

# 16234

# 1. 입력 받기
N, L, R = map(int, input().split())
A = [list(map(int, input().split())) for _ in range(N)]
# print(A)

population_rotate = 0

union_population = 0

count = 2000

while count:
    print("@#@##")
    union_list = []

    print(A)
    # print("0. 여기?")
    for i in range(N - 1):
        for j in range(N - 1):
            condition1 = abs(A[i][j] - A[i][j + 1])
            condition2 = abs(A[i][j] - A[i + 1][j])
            condition3 = abs(A[i][j + 1] - A[i + 1][j + 1])
            if i == 0 and j == 0:  # 처음엔 전부 다 저장하고,
                if condition1 >= L and condition1 <= R:
                    union_list.append((i, j))
                    union_list.append((i, j + 1))
                if condition2 >= L and condition2 <= R:
                    union_list.append((i, j))
                    union_list.append((i + 1, j))

            elif i == N - 2 and j == N - 2:
                if condition1 >= L and condition1 <= R:
                    union_list.append((i, j + 1))
                if condition2 >= L and condition2 <= R:
                    union_list.append((i + 1, j))
                if condition3 >= L and condition3 <= R:
                    union_list.append((i + 1, j + 1))
            else:  # 이후에는 다음거만 저장해서
                if condition1 >= L and condition1 <= R:
                    union_list.append((i, j + 1))
                if condition2 >= L and condition2 <= R:
                    union_list.append((i + 1, j))

    if not union_list:
        print("union_list!!", union_list)
        print("끝")
        break

    else:
        # print("union_list" , union_list)
        union_population = 0
        union_list = list(set(union_list))

        print("union_list", union_list)
        for union in union_list:
            i = union[0]
            j = union[1]
            print("union_population: ", union_population)
            union_population += int(A[i][j])


        population_rotate += 1
        population = union_population // len(union_list)
        for union in union_list:
            i = union[0]
            j = union[1]
            A[i][j] = population

print(population_rotate)
