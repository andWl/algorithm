#
# 5 5
# #####
# #..B#
# #.#.#
# #RO.#
# #####

from sys import stdin
from collections import deque
input = stdin.readline

n, m = map(int, input().split())
a = [list(input().strip()) for _ in range(n)]
check = [[[[False]*m for _ in range(n)] for _ in range(m)] for _ in range(n)]
dx, dy = (-1, 0, 1, 0), (0, 1, 0, -1)
q = deque()

def init():
    _rx, _ry, _bx, _by = [0]*4
    for i in range(n):
        for j in range(m):
            if a[i][j] == 'R':
                _rx, _ry = i, j
            elif a[i][j] == 'B':
                _bx, _by = i, j
    q.append((_rx, _ry, _bx, _by, 0))
    check[_rx][_ry][_bx][_by] = True

def move(_x, _y, _dx, _dy, _c):
    # +dx 일떼 즉 한칸 앞이 벽이거나 현위치가 O인 경우가 아니면 전진!!!
    while a[_x+_dx][_y+_dy] != '#' and a[_x][_y] != 'O':
        _x += _dx
        _y += _dy
        _c += 1
    return _x, _y, _c

def bfs():
    while q:
        rx, ry, bx, by, d = q.popleft()
        if d >= 10:
            break
        for i in range(4):
            # move에서 끝까지 움직이는거다 rc는 총 움직인 칸 nrx nry nbx nby는 한번 쭉 갔을 때 최종 위치
            # 파란색 공 생각하지 않고 일단 움직이는거다.
            nrx, nry, rc = move(rx, ry, dx[i], dy[i], 0)
            nbx, nby, bc = move(bx, by, dx[i], dy[i], 0)

            # 예제 5번 처럼 b가 먼저 빠지는 경우도 있는데 그렇다고 fail은 아니니까 일단 해당 경우는 넘어가는거다
            if a[nbx][nby] == 'O':
                continue
            if a[nrx][nry] == 'O':
                print(d+1)
                return

            # 예제 2번의 경우이다.
            # 위에서 생각없이 움직였으니까 만약에 동일한 위치에 빨간 파란공이 있으면 적게 움직인 놈이 앞에 있는 놈이다. 즉 많이 온놈을 온길에서 한칸만 뒤로 댕기면 됨
            if nrx == nbx and nry == nby:
                if rc > bc:
                    nrx, nry = nrx-dx[i], nry-dy[i]
                else:
                    nbx, nby = nbx-dx[i], nby-dy[i]


            if not check[nrx][nry][nbx][nby]:
                # 모든 가는길을 check 할 필요는 없고 한번 움직였을 때 최종적인 그림의 순간만을 check 하면된다.
                check[nrx][nry][nbx][nby] = True
                q.append((nrx, nry, nbx, nby, d+1))

    # q에 아무것도 없거나 10넘어서 break 걸리면 -1 출력하고 끝내 (이건 예제10을위해필요)
    print(-1)

init()
bfs()
