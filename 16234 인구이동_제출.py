from sys import stdin
import math
from collections import deque
input =stdin.readline
n, l, r = map(int,input().split())
a=[list(map(int,input().split())) for _ in range(n)]

check=[["@"]*n for _ in range(n)]

ans,nei_ct,nei=0,0,[]
q=deque()
dx,dy=(-1,0,1,0),(0,1,0,-1)


def gubun():
    global check
    check_num=0

    for i in range(len(nei)):

        if check[nei[i][0]][nei[i][1]] == "#":
            q.append(nei[i])
            check[nei[i][0]][nei[i][1]] = check_num
        while q:

            t=q.popleft()

            for i in range(4):
                # try:
                x,y=t[0] + dx[i],t[1]+dy[i]
                if x>=0 and  y>=0 and x<n and y<n:
                    if check[t[0]+dx[i]][t[1]+dy[i]]!="#":
                        continue
                    temp = abs(a[t[0]][t[1]] - a[t[0]+dx[i]][t[1]+dy[i]])
                    if temp >= l and temp <= r:
                        check[t[0]+dx[i]][t[1]+dy[i]] = check_num
                        q.append((t[0]+dx[i],t[1]+dy[i]))

        if not check_true():
            break
        check_num+=1
    move(check_num+1)

def check_true():
    for i in range(n):
        for j in range(n):
            if check[i][j]!="#":
                continue
            else:
                return True
    return False

def move(check_num):
    global nei_ct,nei

    for t in range(check_num):
        sum,cnt = 0,0
        for i in range(n):
            for j in range(n):
                if check[i][j] == t:
                    sum += a[i][j]
                    cnt+=1

        sc=math.floor(sum / cnt)

        for i in range(n):
            for j in range(n):
                if check[i][j] == t:
                    a[i][j]=sc
def main():

    global ans,nei_ct,nei,check
    while 1:
        nei = []
        check = [["@"] * n for _ in range(n)]
        for i in range(n):
            for j in range(n-1):
                # 가로 인접 비교
                temp=abs(a[i][j] - a[i][j + 1])
                if  temp>=l and temp<=r :
                    nei.append((i, j))
                    check[i][j]="#"
                    nei.append((i, j+1))
                    check[i][j+1] = "#"
                temp = abs(a[j][i] - a[j+1][i])
                if  temp>=l and temp <=r:
                    nei.append((j,i))
                    check[j][i] = "#"
                    nei.append((j+1,i))
                    check[j+1][i] = "#"
        nei=list(set(nei))
        nei_ct=len(nei)
        if check_true():
            ans+=1
            gubun()
        else:
            print(ans)
            return

main()

