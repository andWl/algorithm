# 4
# 2 0 2 8
# 0 0 2 2
# 0 0 0 0
# 0 0 0 0


from sys import stdin
from collections import deque
# 이렇게 띄어져 있는거 테스트할때 아래놈 쓰니까 런타임 에러 뜬다
# 여기서 readline() 이렇게 쓰면 입력받을때 str not callable 뜬다
input=stdin.readline
# from copy import deepcopy
q=deque()
# 여기서 int 넣어줘야함 그래야 for 돈다
n=int(input())
# 여기서 반드시 list 넣기전에 map으로 int 변환해줘야함.
# map 하면 replace 필요없다
# map 하면 strip  필요없다
# mt=[list(map(int,input().strip())) for _ in range(n)]

print(n)
mt=[list(map(int, input().split())) for _ in range(n)]

check=[[0]*n for _ in range(n)]
# print(check)
# print(mt)

q.append((mt, 0))

# print(q)

def move(_mt, dr):
	change=True
	cc=0
	# 아래처럼 쓰면 안된다. 그러면 콜바이레퍼런스로 돼서 0,0 변경하면 1,0 2,0 다 똑같이 변경됨
	# 	# check = [[0] * n] * n
	# 	# check = [[0] * n for _ in range(n)]
	for i in range(n):
		for j in range(n):
			check[i][j]=0
	# print(check)
	while(change):
		# print("???")
		change=False
		for i in range(n-1):
			#print("i: "+str(i))
			for j in range(n):
				# print("#")
				# _print(check)
				#print("j: "+str(j))
				if dr==0:
					ix=(i,j,i+1,j)
				elif dr==1:
					ix=(j,n-1-i,j,n-2-i)
				elif dr==2:
					ix=(n-i-1,j,n-i-2,j)
				else:	
					ix=(j,i,j,i+1)
				#print("dr: " +str(dr))	
				#뒤가 0인경우
				
				#print(ix[0], ix[1])
				if _mt[ix[2]][ix[3]]==0:
					continue
				#앞이 0인 경우
				# !!!! "0" 이랑 0 은 다르다.... 여기서 막혔었음 
				elif _mt[ix[0]][ix[1]]==0:
					# print("#앞이 0인 경우")
					_mt[ix[0]][ix[1]]=_mt[ix[2]][ix[3]]
					_mt[ix[2]][ix[3]]=0

					check[ix[0]][ix[1]]=check[ix[2]][ix[3]]
					check[ix[2]][ix[3]]=0

					cc+=1
					change=True
					# _print(_mt)
				#앞뒤가0 아니고 다른경우
				elif _mt[ix[0]][ix[1]]!=_mt[ix[2]][ix[3]]:
					continue
				#앞뒤가  0이 아니고 같은 경우
				elif _mt[ix[0]][ix[1]]==_mt[ix[2]][ix[3]] and check[ix[0]][ix[1]] == 0 and check[ix[2]][ix[3]]==0:
				# elif _mt[ix[0]][ix[1]] == _mt[ix[2]][ix[3]]:
					# print("#앞뒤가  0이 아니고 같은 경우")
					#여기서 *=2했더니 string 이라서 같은값 두개로 나옴 
					# print(type(_mt[ix[0]][ix[1]]))
					# print(ix[0], ix[1])
					_mt[ix[0]][ix[1]] =_mt[ix[0]][ix[1]]*2
					_mt[ix[2]][ix[3]]=0
					cc+=1
					change=True
					check[ix[0]][ix[1]]=1
					# _print(_mt)
					# _print(check)
					check[ix[2]][ix[3]] =0
				# else:
				# 	print("!@?#!@#!@#!?@#?")
	return _mt, cc
# def max(_mt):
# 	_max = 0
# 	for i in range(n):
# 		for j in range(n):
# 			# print(_mt[i][j])
# 			if _max<_mt[i][j]:
# 				_max=_mt[i][j]
# 	return _max

def _print(t):
	for i in range(n):
		print(t[i])
#
def bf():	
	_max=0
	while q:
		mt, ct=q.popleft()
		for i in range(4):
			# print(str(i)+" move 전!!")
			# _print(mt)
			# print("ct : "+str(ct))

			# _mt=deepcopy(mt)

			# deepcopy 랑 같어
			_mt = [x[:] for x in mt]

			_mt,cc=move(_mt,i)
			# 너무 중요......
			_ct=ct
			# 테스트 값이 한번 안움직인다고 해서 패쓰하면 안됨
			# [0, 0, 4, 8]
			# [0, 0, 0, 4]
			# [0, 0, 0, 0]
			# [0, 0, 0, 0]
			# 이런값은 위로 할때는 안움직여도 왼쪽으로는 움직일수있기 때문

			if cc!=0:
				# print("!!!")
				_ct+=1
				if ct<5:


					# temp=max(_mt)
					# if _max<temp:
					# 	_max=temp
					# print("append 전!!!")
					# _print(_mt)
					#
					# 이렇게 이차원은 1차원 리스트에서 max 찾고 비교하는 방식으로
					for i in range(n):
						_max=max(_max, max(_mt[i]))


					q.append((_mt,_ct))
				else:
					continue
					
	print(_max)
bf()
























