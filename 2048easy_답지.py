from collections import deque
n = int(input())
a = [list(map(int, input().split())) for _ in range(n)]
ans, q = 0, deque()

print(a)
def get(i, j):
    if a[i][j]:
        q.append(a[i][j])
        a[i][j] = 0

def merge(i, j, di, dj):
    while q:
        x = q.popleft()
        if not a[i][j]:
            a[i][j] = x
        elif a[i][j] == x:
            a[i][j] = x*2
            i, j = i+di, j+dj
        else:
            i, j = i+di, j+dj
            a[i][j] = x

def move(k):
    if k == 0:
        for j in range(n):
            for i in range(n):
                get(i, j)
            merge(0, j, 1, 0)
    elif k == 1:
        for j in range(n):
            for i in range(n-1, -1, -1):
                get(i, j)
            merge(n-1, j, -1, 0)
    elif k == 2:
        for i in range(n):
            for j in range(n):
                get(i, j)
            merge(i, 0, 0, 1)
    else:
        for i in range(n):
            for j in range(n-1, -1, -1):
                get(i, j)
            merge(i, n-1, 0, -1)

# 재귀함수다
def solve(cnt):
    # list 임에도 global을 쓰는 이유는 global 변수를 수정할 일이 있을때  a = [x[:] for x in b] 이거처럼 리스트를 통채로 수정할때 !!
    # 주의할사항은 리스트는 글로벌 선언안해도 안에 값들을 변경가능하다. 콜바이래퍼런스이기 때문. 그러나 그냥 일반 변수는 전역변수 read만 가능하고 수정 불가
    # 수정할때는 global을 반드시 써줘야한다.
    # 여기서도 ans를 미리 위에서 전역으로 선언했지만 일단 제일 첫 회에 비교를 해야하고, solve 함수 return 후에 밖에서 출력을 하려다 보니 이렇게 global로 가져오는거다
    global a, ans
    if cnt == 5:
        for i in range(n):
            ans = max(ans, max(a[i]))
        return
    # 이건 그냥 deep copy 하는거랑 같은 개념
    b = [x[:] for x in a]

    for k in range(4):
        move(k)
        solve(cnt+1)
        a = [x[:] for x in b]



solve(0)
print(ans)
