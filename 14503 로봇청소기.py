from sys import stdin
input = stdin.readline
from time import sleep

n, m = map(int, input().split())
dc=0
nx, ny, d = map(int, input().split())
a=[list(map(int, input().split())) for _ in range(n)]

ans=0
dx, dy= (-1,0,1,0), (0,1,0,-1)
dxy =((0,-1),(-1,0),(0,1),(1,0))
b_dxy = ((1,0),(0,-1),(-1,0),(0,1))


def _print():
    for i in range(n):
        for j in range(m):
            print(a[i][j], end='')
        print()
    sleep(3)
def main():

    global ans, d,nx,ny,dc
    while 1:
        # print(dc)
        # sleep(1)
        # 현위치 청소, back 한 경우는 청소하면 안되니까 필요
        if a[nx][ny]==0:
            ans+=1
            a[nx][ny]=2

        # 이게 방향 전환 보다 위에 있어야해 (편하게 하려고 왼쪽 기준으로 만듬)
        _dx, _dy = dxy[d][0], dxy[d][1]

        # 방향 전환
        d=(d+3)%4

        # 다음 위치 확인
        _nx, _ny = nx+_dx, ny+_dy
        # print(a[_nx][_ny])
        if a[_nx][_ny] == 0:
            # print("!@#!#@#")
            dc=0
            nx,ny=_nx,_ny
            ans+=1
            a[nx][ny]=2

        else:
            # 이동불가 count
            # print("11")
            dc+=1

        # 후진
        back=a[nx+b_dxy[d][0]][ny+b_dxy[d][1]]
        if dc==4 and back == 2:
            dc =0
            nx+=b_dxy[d][0]
            ny+=b_dxy[d][1]

        elif dc==4 and back == 1:
            return

main()
print(ans)

