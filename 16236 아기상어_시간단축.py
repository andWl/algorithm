# 6
# 1 0 0 0 0 0
# 0 0 0 0 0 5
# 9 1 0 5 6 6
# 1 0 0 3 4 5
# 3 2 0 6 5 4
# 6 6 6 6 6 6

from sys import stdin
input = stdin.readline
# import sys
# input = sys.stdin.readline
# sys.setrecursionlimit(10000)

from collections import deque

q=deque()
n=int(input())
a=[list(map(int,input().split())) for _ in range(n)]
# print(n,a)
# 상어 위치
dx,dy,size,eat,ans=(-1,0,1,0),(0,1,0,-1),2,0,0
first_x,first_y = 0,0
def init():
    global first_x, first_y
    for i in range(n):
        for j in range(n):
            # print(i,j)
            if a[i][j]==9:
                first_x,first_y=i,j
                q.append((i,j,0))
                return

init()
def _print():
    for i in range(n):
        for j in range(n):
            print(a[i][j], end=' ')
        print()

def bite(_x,_y,_timer):
    # 아래 생성된 글로벌 변수도 가능한가?

    global x,y,eat,size,ans,first_x,first_y
    # print("bite", first_x,first_y)
    eat+=1
    # 일단 현재 위치는 0으로 만든다
    a[first_x][first_y]=0
    # print(x,y)
    x,y=_x,_y
    if eat % size==0:
        size+=1
        eat=0
    #    먹고 나서는 9 현재 상어 위치
    a[x][y],ans=9,_timer
    # _print()

    # print("x,y,size,eat,_timer",x,y,size,eat,_timer)
    first_x,first_y=x,y
    q.append((x,y,_timer))

def eat_cert(timer_limit):
    for i in range(n):
        for j in range(n):
            if fish_eat[i][j]:
                bite(i, j, timer_limit)
                return True
    return False

# def finding_fish():
# init()
while 1:
    # print("init", first_x, first_y)
    # print("!@#!@#@#!@#!@#!@#!@#!#")
# 이거 한바퀴 돌면 한마리 먹는거다
    check = [[False]*n for _ in range(n)]
    fish_eat = [[False] * n for _ in range(n)]
    timer_limit=1e9
    while q:

        # 이렇게 바로 대입 가능
        x,y,timer=q.popleft()

        if timer == timer_limit:
            q.clear()
            break
        # print(x,y,timer)

        for i in range(4):
            # _x,_y는 이동하고자 하는 위치
            _x,_y = x + dx[i], y + dy[i]
            if _x>=0 and _x<n and _y>=0 and _y<n:
                # 같거나, 0(빈공간), 먹을수 있는 생선이거나
                # check 하면 안된다.
                if a[_x][_y] <= size and not check[_x][_y]:
                    # 이동을 한번 한거다 먹든 말든
                    q.append((_x,_y,timer+1))
                    check[_x][_y]=True

                    # 먹기 위한 조건
                    if a[_x][_y] < size and a[_x][_y] >0:
                        # fish.append((_x,_y,timer+1))
                        timer_limit=timer+1
                        fish_eat[_x][_y]=True
                        # print("timer_limit: " , timer_limit)

    if not eat_cert(timer_limit):
        print(ans)
        break
