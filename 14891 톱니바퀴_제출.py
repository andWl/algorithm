##백준 14891 톱니바퀴
from sys import stdin
input=stdin.readline

aa=[list(input().split()) for _ in range(4)]

a, ans=[],0
for i in range(4):
    b=list(map(int, aa[i][0]))
    a.append(b)

rc=int(input())
num_d=[list(map(int, input().split())) for _ in range(rc)]

dir=[100]*4
r=[False]*3

def rotate():
    global a
    for i in range(4):
        if dir[i] == 1:
            temp=a[i].pop()
            a[i].insert(0,temp)

        elif dir[i] == -1:
            first=a[i][0]
            a[i].append(first)
            a[i].pop(0)
        else:
            continue

def relation():
    global r
    for i in range(3):
        if a[i][2] == a[i + 1][6]:
            r[i] = True
        else:
            r[i] = False

def direction(_t,_d):
    global dir
    temp_t = _t - 1
    dir[temp_t] = _d

    while temp_t  > 0:
        if r[temp_t - 1] or dir[temp_t]==0:
            dir[temp_t-1] = 0
        else:
            dir[temp_t-1] = dir[temp_t] * -1
        temp_t -= 1

    temp_t=_t-1
    while temp_t < 3:
        if r[temp_t] or dir[temp_t]==0:
            dir[temp_t+1] = 0
        else:
            dir[temp_t+1] = dir[temp_t] * -1
        temp_t += 1

def main():
    for i in range(rc):
        relation()
        direction(num_d[i][0],num_d[i][1])
        rotate()
main()

for i in range(4):
    ans+=a[i][0]*pow(2,i)
print(ans)