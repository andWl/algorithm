from sys import stdin
input = stdin.readline



n,m = map(int, input().split())
a = [list(map(int, input().split())) for _ in range(n)]

dx, dy = (-1, 0 , 1, 0) , (0, 1, 0 , -1)
vs = [[0]*m for _ in range(n-2)]
ps = [[0]*(m-2) for _ in range(n)]

for i in range(n-2):
    for j in range(m):
        vs[i][j] = a[i][j] + a[i + dx[2]][j + dy[2]] + a[i + 2 * dx[2]][j + 2 * dy[2]]

for i in range(n):
    for j in range(m-2):
        ps[i][j] = a[i][j] + a[i + dx[1]][j + dy[1]] + a[i + 2 * dx[1]][j + 2 * dy[1]]


def ss():
    sum=[]
    # 네모
    for i in range(n-1):
        for j in range(m-1):
            # 매번 max 가 빠른지 한번에 max가 빠른지 비교
            sum.append(a[i][j]+a[i+dx[1]][j+dy[1]]+a[i+dx[2]][j+dy[2]]+a[i+dx[2]+dx[1]][j+dy[2]+dy[1]])
    # print(sum)
    # vs (1자 3개)
    for i in range(n-2):
        for j in range(m):
            #세로
            if i+3*dx[2] < n:
                sum.append(vs[i][j]+a[i+3*dx[2]][j+3*dy[2]])

    #      니은자
            if j+2*dy[2]+dy[1] < m:
                sum.append(vs[i][j] + a[i + 2 * dx[2] +dx[1]][j + 2 * dy[2]+dy[1]])
            # 니은 대칭
            #     #
            if j+2*dy[2]+dy[3] >= 0:
                sum.append(vs[i][j] + a[i + 2 * dx[2] +dx[3]][j + 2 * dy[2]+dy[3]])

            # 기억
            if j+ dy[3] >= 0:
                sum.append(vs[i][j] + a[i + dx[3]][j + dy[3]])
            #

            # # 기억 대칭
            if j+ dy[1] < m:
                sum.append(vs[i][j] + a[i + dx[1]][j + dy[1]])
            # if

            #         뻐큐 모양 인데 오른쪽 으로
            if j + dy[1] + dy[2] < m:
                sum.append(vs[i][j] + a[i + dx[1]+dx[2]][j + dy[1] + dy[2]])

            #         뻐큐 모양 인데 왼쪽 으로
            if j + dy[3] + dy[2] >= 0 :
                sum.append(vs[i][j] + a[i + dx[3]+dx[2]][j + dy[3] + dy[2]])
    # print(sum)
    # ps 가로 1자 3칸
    for i in range(n):
        for j in range(m-2):
            # # 세로
            if j+3*dy[1] < m:
                sum.append(ps[i][j]+a[i+3*dx[1]][j+3*dy[1]])

         # 니은자
            if i+2*dx[1]+dx[2] < n:
                sum.append(ps[i][j] + a[i + 2 * dx[1] +dx[2]][j + 2 * dy[1]+dy[2]])
            # 니은 대칭
            #     #
            if i+dx[2] < n:
                sum.append(ps[i][j] + a[i + dx[2]][j+dy[2]])

            # 기억
            if i+ dx[0] >= 0:
                sum.append(ps[i][j] + a[i + dx[0]][j + dy[0]])


            # # 기억 대칭
            if i+ 2*dx[1] +dx[0] >= 0:
                sum.append(ps[i][j] + a[i + 2 * dx[1] +dx[0]][j + 2 * dy[1]+dy[0]])


            #         뻐큐 모양 인데 오른쪽 으로
            if i + dx[1] + dx[2] < n:
                sum.append(ps[i][j] + a[i + dx[1]+dx[2]][j + dy[1] + dy[2]])

            #         뻐큐 모양 인데 왼쪽 으로
            if i + dx[1] + dx[0] >= 0 :
                sum.append(ps[i][j] + a[i + dx[1]+dx[0]][j + dy[1] + dy[0]])

    # print(sum)
    print(max(sum))


def main():
    if (n >=4 and n <=500) and (m>=4 and m<=500) :
        ss()
main()
