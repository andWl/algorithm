## 백준 14503 로봇청소기
from sys import stdin
input = stdin.readline

n, m = map(int, input().split())
nx, ny, d = map(int, input().split())
a=[list(map(int, input().split())) for _ in range(n)]
dx, dy, dxy,b_dxy, dc, ans = (-1,0,1,0), (0,1,0,-1), ((0,-1),(-1,0),(0,1),(1,0)),((1,0),(0,-1),(-1,0),(0,1)),0,0

def main():
    global ans, d,nx,ny,dc
    while 1:
        if a[nx][ny]==0:
            ans+=1
            a[nx][ny]=2
        _dx, _dy = dxy[d][0], dxy[d][1]
        d, _nx, _ny = (d+3)%4, nx+_dx, ny+_dy
        if a[_nx][_ny] == 0:
            dc, nx,ny,a[nx][ny]=0, _nx,_ny,2
            ans+=1
        else:
            dc+=1
        back=a[nx+b_dxy[d][0]][ny+b_dxy[d][1]]
        if dc==4 and back == 2:
            dc =0
            nx+=b_dxy[d][0]
            ny+=b_dxy[d][1]
        elif dc==4 and back == 1:
            return
main()
print(ans)

