from sys import stdin
input=stdin.readline

# 0포함 받아야해 붙어있는거 어케 받음
# 이렇게 해서 하나씩 따로 못받았음 바보 () 하나빼야지
aa=[list(input().split()) for _ in range(4)]

# 리스트를 int 로 바꾸는법
a, ans=[],0
for i in range(4):
    b=list(map(int, aa[i][0]))
    a.append(b)

# list(map(int, list_a))
#
# 출처: https://shayete.tistory.com/entry/리스트의-문자열을-int-형태로-변환 [샤의 공간]


# for i in range(4):
#     for j in range(8):
#         a[i][j]=int(a[i][j])

rc=int(input())
num_d=[list(map(int, input().split())) for _ in range(rc)]

dir=[100]*4
# 관계
r=[False]*3
# print(dir)


# 이거 밖에서 for i (4) 로 돌려 아니지 이미 위에서 방향은 다 정해졌으니 그냥 돌리면 됨
def rotate():
    # global 안써서 실수
    global a
    # 시계방향
    for i in range(4):
        if dir[i] == 1:
            temp=a[i].pop()
            # append 반대는 insert 0 번째 순서에 temp 넣는거
            a[i].insert(0,temp)

        elif dir[i] == -1:
            first=a[i][0]
            a[i].append(first)
            # 0번째를 삭제
            a[i].pop(0)
        #     이건 없어도 됨 여기가 정지 일때 (0)
        else:
            continue

# 극 관계가 같은지 다른지
def relation():
    global r
    for i in range(3):
        if a[i][2] == a[i + 1][6]:
            r[i] = True
        else:
            r[i] = False

# target, direction
def direction(_t,_d):
    global dir
    temp_t = _t - 1
    dir[temp_t] = _d

    # 실제 _t보다 -1로 인덱싱 해야함

    # 왼쪽으로 일단 한번 dir 방향 찾음 ex 3 일때 2,1 번째 바퀴 방향
    while temp_t  > 0:
        # 같거나 안도는 경우
        if r[temp_t - 1] or dir[temp_t]==0:
            dir[temp_t-1] = 0
        # 다르면 기준이 되는 바퀴의 반대 방향으로 돌아 근데 안 안돌면 무의미함
        else:
            dir[temp_t-1] = dir[temp_t] * -1
        # 안도는 경우


        temp_t -= 1

    temp_t=_t-1
    while temp_t < 3:
        # 같으면 정지
        if r[temp_t] or dir[temp_t]==0:
            dir[temp_t+1] = 0
        # 다르면 기준이 되는 바퀴의 반대 방향으로 돌아
        else:
            dir[temp_t+1] = dir[temp_t] * -1
        temp_t += 1


def main():
    for i in range(rc):
        relation()
        direction(num_d[i][0],num_d[i][1])
        rotate()

main()


for i in range(4):
    # print(a[i][0])
    ans+=a[i][0]*pow(2,i)

print(ans)