##백준 3190 뱀
from sys import stdin
input=stdin.readline

n=int(input())
mt=[[0]*n for _ in range(n)]
ac=int(input())

for _ in range(ac):
   adx, ady= map(int, input().split())
   mt[adx-1][ady-1]=1

dc=int(input())
dmt=[list(input().split()) for _ in range(dc)]

for i in range(dc):
	dmt[i][0]=int(dmt[i][0])
	if dmt[i][1]=="D":
		dmt[i][1]=0
	else:
		dmt[i][1]=1

dl=((1,2,3,0),(3,0,1,2))
d=((-1,0,1,0),(0,1,0,-1))
g=[[0,0,1,0]]

def out(_direction):
	if g[0][0] < 0 or g[0][0] > n-1 or g[0][1] <0 or g[0][1]>n-1:
		return True
	else:
		return False

def crash():
	for i in range(len(g)):
		if i==0:
			continue
		if g[0][0]==g[i][0] and g[0][1]==g[i][1]:
			return True
	return False

def move():
	dmt_i=0
	end=False
	while(1):
		add=False
		for i in range(len(g)):
			if end==True:
				end=False
				break

			direction=g[i][2]
			dx,dy=d[0][direction],d[1][direction]
			g[i][0]+=dx
			g[i][1]+=dy
			g[i][3]+=1

			if i==0 and (out(direction) or crash()):
				print(g[i][3])
				return

			_direction=direction

			for dd in range (len(dmt)):
				if dmt[dd][0]==g[i][3]:
					direction = dl[dmt[dd][1]][direction]
					if dmt_i+1 < dc:
						dmt_i+=1
			g[i][2]=direction

			if i==0:
				if mt[g[0][0]][g[0][1]]==1:
					add=True
					mt[g[0][0]][g[0][1]]=0
			if i==len(g)-1 and add==True:
					g.append([g[i][0]-dx, g[i][1]-dy, _direction, g[-1][3]-1])
					add=False
					end=True
move()



