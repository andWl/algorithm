##백준 14888 연산자 끼워넣기
from sys import stdin
import math
from itertools import permutations
input=stdin.readline
n=int(input())
num=list(map(int,input().split()))
cal=list(map(int,input().split()))
ma_ans,mi_ans,l_cal =-1e9,1e9,[]

for i in range(len(cal)):
    for _ in range(cal[i]):
        l_cal.append(i)
t_cal=list(permutations(l_cal,len(l_cal)))
set_t_cal=list(set(t_cal))

def calc(_l_cal):
    global ma_ans, mi_ans
    s=num[0]
    for i in range(len(_l_cal)):
        if _l_cal[i]==0:
           s += num[i + 1]
        elif _l_cal[i]==1:
            s -= num[i + 1]
        elif _l_cal[i] == 2:
            s *= num[i + 1]
        else:
            if s<0:
                s=math.ceil(s/num[i+1])
            else:
                s=math.floor(s/num[i+1])
    ma_ans,mi_ans =max(ma_ans,s),min(mi_ans,s)

def main():
    for i in range(len(set_t_cal)):
        calc(set_t_cal[i])
main()
print(ma_ans)
print(mi_ans)