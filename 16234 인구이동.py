from sys import stdin
import math
from collections import deque
input =stdin.readline
n, l, r = map(int,input().split())
a=[list(map(int,input().split())) for _ in range(n)]

# 아래 @를 false 로 하면 위험한게 check에 나중에 0,1 이 들어간다면 false 랑 0 이랑 같은걸로 판단해버림
# True도 마찬가지
check=[["@"]*n for _ in range(n)]

ans,nei_ct,nei=0,0,[]
q=deque()
dx,dy=(-1,0,1,0),(0,1,0,-1)


def gubun():
    global check
    check_num=0

    for i in range(len(nei)):
        # print(nei)
        # print(nei[i])
        # print(nei[i][0])

        if check[nei[i][0]][nei[i][1]] == "#":
            q.append(nei[i])
            check[nei[i][0]][nei[i][1]] = check_num
        while q:
            # print(1)
            # popleft로 해야한다. 주의!!!!
            t=q.popleft()
            # print(t)
            for i in range(4):
                # 범위가 넘어가더라도 -1 같은 경우는 a[-1] 은 a의 끝을 의미해서 음수는 처리해줘야함
                try:
                    if t[0]+dx[i]>=0 and t[1]+dy[i] >=0 :
                        # 이미체크한건빠이
                        if check[t[0]+dx[i]][t[1]+dy[i]]!="#":
                            continue
                        temp = abs(a[t[0]][t[1]] - a[t[0]+dx[i]][t[1]+dy[i]])
                        # print(temp)
                        if temp >= l and temp <= r:
                        # if check[t[0]+dx[i]][t[1]+dy[i]]==True:
                            check[t[0]+dx[i]][t[1]+dy[i]] = check_num
                            q.append((t[0]+dx[i],t[1]+dy[i]))
                except IndexError:
                    continue
        if not check_true():
            break
        check_num+=1

    # _print_check()
    move(check_num+1)

def check_true():
    for i in range(n):
        for j in range(n):
            if check[i][j]!="#":
                continue
            else:
                return True
    return False

def move(check_num):
    global nei_ct,nei

    # print(nei)
    for t in range(check_num):
        sum,cnt = 0,0
        for i in range(n):
            for j in range(n):
                if check[i][j] == t:
                    sum += a[i][j]
                    cnt+=1

        sc=math.floor(sum / cnt)

        for i in range(n):
            for j in range(n):
                if check[i][j] == t:
                    a[i][j]=sc
    # _print_check()

def _print_check():
    for i in range(n):
        for j in range(n):
            print(check[i][j], end=' ')
        print()
def _print():
    for i in range(n):
        for j in range(n):
            print(a[i][j], end=' ')
        print()
def main():

    global ans,nei_ct,nei,check
    while 1:
        # print(1)
        # _print()
        # 글로벌이라도 다시 여기서 초기화 해버리면 이건 그냥 이름은 같은 지역 변수가 되어버림 그래서 위에 글로벌 선언을 해줘야함
        nei = []
        check = [["@"] * n for _ in range(n)]
        for i in range(n):
            for j in range(n-1):
                # 가로 인접 비교
                temp=abs(a[i][j] - a[i][j + 1])
                if  temp>=l and temp<=r :
                    nei.append((i, j))
                    check[i][j]="#"
                    nei.append((i, j+1))
                    check[i][j+1] = "#"
                # 세로 인접 비교
                temp = abs(a[j][i] - a[j+1][i])
                if  temp>=l and temp <=r:
                    nei.append((j,i))
                    check[j][i] = "#"
                    nei.append((j+1,i))
                    check[j+1][i] = "#"
        # _print_check()
        nei=list(set(nei))
        # print(nei)
        nei_ct=len(nei)
        # print("nei_ct" ,nei_ct)
        if check_true():

            ans+=1

            gubun()
            # _print()
        else:
            print(ans)
            return

main()

# ans=len(nei)
# for i in range(len(nei)):
#     for j in range(i+1, len(nei)):
#         for k in range(2):
#
#             print(nei[i])
#             print(nei[j])
#             if nei[i][k] in nei[j]:
#                 print("같다!!")
#
#                 ans-=1
#                 break
#                 print()


